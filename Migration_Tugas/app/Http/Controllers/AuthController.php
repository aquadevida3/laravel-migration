<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('page.register');
    }

    public function sent(Request $req){
        //dd($req->all()); buat nampilin array apakah inputan sudah berhasil dan benar
        $namaawal = $req->nama;
        $namaakhir = $req->lastnama;
        return view('page.welcome', compact('namaawal', 'namaakhir'));
    }
}
